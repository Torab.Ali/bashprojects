**Home Directory Backup Script**

This script automates the process of creating backups for the home directory and managing older backups to conserve disk space. It's designed to run on Unix-like systems using the bash shell.

**Usage**
Clone the Repository: Clone this repository to your local machine using Git.

**Edit Configuration (Optional):**

 Open the script file backup_home.sh and modify the BACKUP_DIR variable to specify the directory where you want to store the backups.

**Run the Script:**

 Execute the script backup_home.sh using bash. The script will create a compressed archive of your home directory (/home) and save it to the specified backup directory with a timestamp appended to the filename.

**Automatic Backup:**

 You can schedule this script to run periodically using cron or any other task scheduler to ensure regular backups of your home directory.

**Features**

**Backup Creation:** Creates a compressed archive (tar.gz) of the home directory.

**Timestamped Backups:** Each backup filename includes a timestamp for easy identification.

**Automated Deletion:** Deletes backups older than 30 days to prevent disk space exhaustion.

**Script Explanation**

**backup_home.sh: **This is the main script file responsible for the backup process.

**BACKUP_DIR: **Specifies the directory where backups will be stored.

**tar:** Utilizes the tar command to create compressed archives of the home directory.

**find:** Uses the find command to locate and delete older backup files based on their modification date.

**Requirements**

Unix-like operating system (Linux, macOS, etc.).
Bash shell.

**License**
This project is licensed under the MIT License.