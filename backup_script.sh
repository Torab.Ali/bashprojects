#!/bin/bash
#This script make the backup of home dir in backup dir!

#Defining backup file path
BACKUP_DIR='/backups'
BACKUP_FILE="$BACKUP_DIR/home-$(date +%Y%m%d-%H%M%S).tar.gz"

#create a backup of the home dir
tar -czvf "$BACKUP_FILE" /home
echo "backup of the home directory completed successfully"

#Delete backup during after 30 days
echo "deleting backups older than 30 days"
sudo find $BACK_DIR -type f -name 'home-*.tar.gz' -mtime +30 -exec rm -f {} \;

